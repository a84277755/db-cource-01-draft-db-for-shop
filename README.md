# HW5

Demo data:

```
INSERT INTO products.product (title, description, imgUrl, price) VALUES
  ('bread','delicious and a lot of calories', NULL, 1.15),
  ('milk','allergy is unavoidable', NULL, 1),
  ('water','part of a water diet', NULL, NULL),
  ('fork','instrument for solid food', 'https://some.com/fork.jpg', 2.15),
  ('spoon','instrument for liquid food', 'https://some.com/spoon.jpg', 3.25),
  ('knife','instrument for cutting', 'https://some.com/knife.jpg', NULL);

INSERT INTO orders.fixedproductdetails (productId, fixedPrice, fixedDiscount, quantity) VALUES
  (1, 1.00, 0.15, 1),
  (1, 1.15, NULL, 2),
  (1, 1.30, NULL, 1),
  (2, 1.10, 0.05, 1),
  (2, 1.15, NULL, 1),
  (3, 0, NULL, 1),
  (3, 0.15, NULL, 1),
  (6, 1, NULL, 1),
  (6, 2, 1, 2);
```

1) RegExp

```
SELECT id, title, description FROM products.product WHERE description ~~ '%part%';
SELECT id, title, description FROM products.product WHERE description !~~	 '%part%';
```

2) LEFT JOIN + INNER JOIN

LEFT:
```
SELECT products.product.id as id, title, description, quantity, price, fixedPrice
FROM products.product
LEFT JOIN orders.fixedproductdetails 
ON products.product.price = orders.fixedproductdetails.fixedPrice;
```

INNER JOIN
```
SELECT products.product.id as id, title, description, quantity, price, fixedPrice
FROM products.product
INNER JOIN orders.fixedproductdetails 
ON products.product.price = orders.fixedproductdetails.fixedPrice;
```

For LEFT JOIN we have recordings with NULL, for INNER JOIN we don't have recordings with null (for price and fixedPrice)

3) Information about inserted data

```
INSERT INTO products.product (title, description, imgUrl, price) VALUES
  ('bread','delicious and a lot of calories', NULL, 5),
  ('milk','allergy is unavoidable', NULL, 4),
  ('water','part of a water diet', NULL, 3)
returning title, price;
```

4) Update from

```
UPDATE orders.fixedproductdetails
SET fixedPrice = 0
FROM products.product
WHERE CAST (price as numeric) > 1.00;
```

5) DELETE

```
DELETE FROM orders.fixedproductdetails
	USING products.product
	WHERE CAST(price as numeric) > 0 AND CAST (fixeddiscount as numeric) < 0;
```

# HW4

## Create DB
1) Create DB and connect:
```
CREATE DATABASE Otus;
\c otus
```

2) Create app roles:

```
CREATE ROLE commonUser WITH login;
CREATE ROLE admin WITH login;

```

3) Create schemas:

```
create schema products;
create schema users;
create schema orders;
```

4) Create tables:

```
CREATE TABLE products.category(
  id serial PRIMARY KEY,
  title varchar(255),
  description text,
  imgUrl text
)
CREATE TABLE products.manufacture(
  id serial PRIMARY KEY,
  title varchar(255),
  description text
)
CREATE TABLE products.supplier(
  id serial PRIMARY KEY,
  title varchar(255),
  description text,
  address varchar(255),
  phone varchar(11),
  email varchar(255),
  siteUrl varchar(255)
)
CREATE TABLE products.product(
  id serial PRIMARY KEY,
  title varchar(255),
  description text,
  imgUrl text,
  price money
)
CREATE TABLE orders.fixedproductdetails(
  id bigserial PRIMARY KEY,
  productId integer,
  fixedPrice money,
  fixedDiscount money,
  quantity integer,
  CONSTRAINT fk_productId
    FOREIGN KEY(productId)
      REFERENCES products.product(id)
)
CREATE TABLE orders.order(
  id serial PRIMARY KEY,
  description text,
  comment text,
  address varchar(255),
  phone varchar(11),
  deliverDate date,
  orderDate date NOT NULL DEFAULT CURRENT_DATE
)
CREATE TABLE users.user(
  id serial PRIMARY KEY,
  name varchar(100),
  email varchar(255),
  password varchar(255),
  imgUrl text,
  phone varchar(11),
  comment text
)
```


# HW2

## Analysis for potential requests\reports\searching.

### Tables

* **Category**
  * Description of fields:
    * **Id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **title** - Can be used for cases:
      * (REPORT) Get all categories
    * **description** - can't be used as search parameter, nothing specific. Can be used as a part of view.
    * **imgUrl** - relative path to img
  * Indexes:
    * ID - PK
      * cardinality = N
* **Product**
  * Description of fileds:
    * **Id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **Title** - name of a product. Can be used for cases:
      * Get list of products of some category (as part of product info)
      * Get list of all products (as part of product info)
      * Search by product name
    * **description** - used for providing some specific information for a product (such as dimensions etc.)
    * **imgUrl** - relative path to img
    * **price** - current price of a product (for all categories)
  * Indexes:
    * ID - PK
      * Cardinality = N
    * title - full text index
      * Cardinatily = N
* **Manufacture**
  * Description of fileds:
    * **id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **title** - name of manufacture. Can be used for cases:
      * Get all manufactures
      * Search by part of name
    * **description** - some specific details, such as country etc.
  * Indexes:
    * **id** - PK
      * Cardinality = N
    * **title** - full text index
      * Cardinality = N
* **Supplier**
  * Description of fieds:
    * **id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **title** - name of supplier. Can be used for cases:
      * Get all suppliers
      * Search by part of name
    * **description** - some specific details, such as comments for previous orders or additional email and phone.
    * **address** - Where is located a supplier
    * **phone** - main phone of supplier. Can be used for searching by phone (partial\full value).
    * **email** - main email for supplier. Can be used for searching by email (partial\full value).
    * **siteUrl** - home page of a company.
  * Indexes:
    * **id** - PK
      * Cardinality = N
    * **title** - full text index
      * Cardinality = N
    * **phone** - full text index
      * Cardinality = N
    * **email** - full text index
      * Cardinality = N
* **FixedProductDetails**
  * Description of fields:
    * **id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **productId** - FK for **Product** table (id field)
    * **fiexedPrice** - which price was used for ordering
    * **fixedDiscount** - how much money were reduced as a discount
    * **quantity** - how many products were booked in an order
  * Indexes:
    * **id** - PK
      * Cardinality = N
* **Order**
  * Description of fileds:
    * **id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **description** - comments for an order
    * **address** - location for delivery. Can be used in search by parts.
    * **phone** - phone, which was used for an order. Can be used in search by partial\full value.
    * **deliverDate** - when it was delivered
    * **orderDate** - when it was ordered
  * Indexes:
    * **id** - PK
      * Cardinality = N
    * **address** - full text index
      * Cardinality = N
    * **phone** - full text index
      * Cardinality = N
* **User**
  * Description of fields:
    * **id** - primary key, auto increment. Will be used for most of basic operations (CRUD) as id.
    * **name** - user name (first name + last name). Can be used for search by partial\full value.
    * **email** - user's email, can be used for search by partial\full value. Can be used for authentication.
    * **password** - hashed password. Can be used for authentication.
    * **imgUrl** - relative path to user's profile picture.
    * **phone** - main phone of user. Can be used for searching by phone (partial\full value).
  * Indexes:
    * **id** - PK
      * Cardinality = N
    * **email - password** - Complex index (for faster authentication)
      * Cardinality = N
    * **phone** - full text index
      * Cardinality = N
    * **email** - full text index, Unique index
      * Cardinality = N
    * **name** - full text index
      * Cardinality = N


### Typical constraints
* phone
  * Not null
  * Length = 10
* quantity
  * MORE OR EQUAL TO 1
* price, fixedPrice
  * MORE OR EQUAL 1
* fixedDiscount
  * MORE OR EQUAL 0
* email
  * Length > 1

# HW1

## Essentials

* **Product** - description of a product for showcase
* **Category** - list of products grouped with one goal\meaning
* **Manufacture** - who generates a product (brand)
* **Supplier** - who can provide a product
* **Order** - all neccessary (to deliver) information about an order
* **FixedProductDetails** - when order is created - we fix all temporary details there (such as price and discount)
* **User** - consumer of our shop

## Relations

* One-to-many
  * Manufacture - products
  * Product - fixed product details
  * Order - fixed product details
  * User - orders
* Many-to-many
  * Product - Category
  * Supplier - manufacture

## Business cases

* Authentication
  * User enters his email and password. Back-end generates a sha-256 hash from input password and checks, if user with such name and hash exists.
* User want to receive a list of products from the same category
  * We can make a search through relation many-to-many **CategoriesProducts** to recieve a list of products for a category.
* User want to receive a list of products of a brand.
  * We can make a search through relation many-to-many **ManufactureProducts** to recieve a list of products for a brand.
* User want to check, which suppliers exist in his city
  * We can make a search through relation many-to-many **ManufactureProducts** and user can all suppliers.
* User want to receive a list of his orders
  * We can search using one-to-many relations **UserOrders**. We will receive a list of order ids. After that we can use relation one-to-many **OrderedProductDetails** and generate a list of orders and products + prices and discounts.
* User want to make an order
  * We need to create all records from user's cart (TODO) and put them into **FixedProductDetails** table. Also we need to create a record in **Order** table with all deliver and contact details.

## Replication

* As we will not have high load we can use only one **Master** and two **Slave** node.
* All readings and writings will be through **Master** node.

## Reserve copying

* As we will not have a high load and huge ammount of data we can make a incrementail copy of data base each 6 hours.
* We need to put these copies to 2 different storages (to reduce a chance of failure)
